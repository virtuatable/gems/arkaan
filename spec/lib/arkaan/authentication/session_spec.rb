RSpec.describe Arkaan::Authentication::Session do
  describe :session_id do
    it 'returns the right token for a given session' do
      expect(build(:session).session_id).to eq 'session_token'
    end
    it 'fails at validating the session if the token is not given' do
      expect(build(:session, session_id: nil).valid?).to be false
    end
    it 'fails at validating the session if the token is not uniq' do
      create(:session)
      expect(build(:session, session_id: 'session_token').valid?).to be false
    end
    it 'fails at validating the session if the token is less than ten characters' do
      expect(build(:session, session_id: 'token').valid?).to be false
    end
  end

  describe :duration do
    it 'returns the right duration for a session' do
      expect(build(:session, account: build(:account), duration: 1000).duration).to be 1000
    end
    it 'returns the right default value for the duration' do
      expect(build(:session, account: build(:account)).duration).to be 86400
    end
    it 'fails at validation the session if the duration is not given' do
      expect(build(:session, account: build(:account), duration: nil).valid?).to be false
    end
    it 'validates the session if the duration is zero' do
      expect(build(:session, account: build(:account), duration: 0).valid?).to be true
    end
    it 'fails at validation the session if the duration is negative' do
      expect(build(:session, account: build(:account), duration: -1).valid?).to be false
    end
  end

  describe :expired? do
    it 'returns false if the session is not expired' do
      payload = {
        account: build(:account),
        created_at: DateTime.now,
        duration: 3600
      }
      expect(build(:session, payload).expired?).to be false
    end
    it 'returns true if the session is expired' do
      payload = {
        account: build(:account),
        created_at: DateTime.now - (2.0/24),
        duration: 3600
      }
      expect(build(:session, payload).expired?).to be true
    end
    it 'returns false if the session is never to be expired' do
      payload = {
        account: build(:account),
        created_at: DateTime.now - (2.0/24),
        duration: 0
      }
      expect(build(:session, payload).expired?).to be false
    end
  end

  describe :account do
    it 'returns the right account for a given session' do
      expect(build(:session, account: build(:account)).account.username).to eq 'Babausse'
    end
  end

  describe :messages do
    it 'returns the right message if the token is not given' do
      session = build(:session, session_id: nil)
      session.validate
      expect(session.errors.messages[:session_id]).to eq(['required'])
    end
    it 'returns the right message if the token is too short' do
      session = build(:session, session_id: 'token')
      session.validate
      expect(session.errors.messages[:session_id]).to eq(['minlength'])
    end
    it 'returns the right message if the token is already taken' do
      create(:session)
      session = build(:session)
      session.validate
      expect(session.errors.messages[:session_id]).to eq(['uniq'])
    end
    it 'returns the right message if the duration is not valid' do
      session = build(:session, duration: -1)
      session.validate
      expect(session.errors.messages[:duration]).to eq(['minimum'])
    end
  end
end