RSpec.describe Arkaan::Authentication::Device do
  let!(:account) { create(:account) }
  let!(:session) { create(:session) }

  describe :user_agent do
    it 'Returns the user agent representation string correctly' do
      device = Arkaan::Authentication::Device.new(sessions: [session], user_agent: 'test')
      expect(device.user_agent).to eq 'test'
    end
  end

  describe :ip do
    it 'Returns the IP address correctly' do
      expect(Arkaan::Authentication::Device.new(sessions: [session], ip: 'test').ip).to eq 'test'
    end
  end
end