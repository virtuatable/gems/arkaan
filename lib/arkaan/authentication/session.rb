# frozen_string_literal: true

module Arkaan
  module Authentication
    # A session represents the connection of the user on our frontend application. Nobody else than our frontend should
    # have access to the session or it's content (in particular to the token), instead they shall use OAuth2.0.
    # A session shall ONLY be created by a premium application (only our frontend applications are premium).
    # @author Vincent Courtois <courtois.vincent@outlook.com>
    class Session
      include Mongoid::Document
      include Mongoid::Timestamps

      # @!attribute [rw] token
      #   @return [String] the unique token for this session, used to identify it and be sure the user is connected.
      field :session_id, type: String
      # @!attribute [rw] websocket_id
      #   @return [String] the ID of the websocket the user is connected to.
      #     It's not an association because instances are embedded.
      field :websocket_id, type: String, default: ''
      # @!attribute [rw] duration
      #   @return [Integer] the duration of the session in seconds before it expires.
      field :duration, type: Integer, default: 86_400

      # @!attribute [rw] account
      #   @return [Arkaan::Account] the account connected to the application.
      belongs_to :account, class_name: 'Arkaan::Account', inverse_of: :sessions
      # @!attribute [rw] device
      #   @return [Arkaan::Authentication::Device] the device (computer/mobile) linked to this session
      belongs_to :device, class_name: 'Arkaan::Authentication::Device', inverse_of: :sessions, optional: true

      validates :session_id,
                presence: { message: 'required' },
                uniqueness: { message: 'uniq', if: :session_id? },
                length: { minimum: 10, message: 'minlength', if: :session_id? }

      validates :duration,
                numericality: { greater_than_or_equal_to: 0, message: 'minimum' },
                presence: { message: 'required' }

      # Checks if the session is expired (it has a duration, and the duration has passed)
      # @return [Boolean] TRUE if the session is expired, FALSE otherwise.
      def expired?
        duration != 0 && created_at + duration.to_f < DateTime.now
      end
    end
  end
end
