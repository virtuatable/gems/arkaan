# frozen_string_literal: true

module Arkaan
  module Authentication
    # A device is a computer or mobile phone from which a user logs in.
    # @pauthor Vincent Courtois <courtois.vincent@outlook.com>
    class Device
      include Mongoid::Document
      include Mongoid::Timestamps

      # @!attribute [rw] label
      #   @return [String] the label attached to the device, describing it in details.
      field :label, type: String
      # @!attribute [rw] user_agent
      #   @return [String] the string representation of the browser and OS of the user
      field :user_agent, type: String
      # @!attribute [rw] ip
      #   @return [String] the IP address of the user
      field :ip, type: String
      # @!attribute [rw] safe
      #   @return [Boolean] TRUE if the device is considered safe, FALSE otherwise
      field :safe, type: Boolean, default: false

      # @!attribute [rw] sessions
      #   @return [Array<Arkaan::Authentication::Session] the sessions this browser is linked to.
      has_many :sessions, class_name: 'Arkaan::Authentication::Session', inverse_of: :device
    end
  end
end
