# frozen_string_literal: true

module Arkaan
  module Concerns
    # Includes the slug field, always the same in all models.
    # @author Vincent Courtois <courtois.vincent@outlook.com>
    module Sluggable
      extend ActiveSupport::Concern

      # Module holding the class methods for the classes including this concern.
      # @author Vincent Courtois <courtois.vincent@outlook.com>
      module ClassMethods
        # Add the field and its validations in the model including it.
        def make_sluggable
          # @!attribute [rw] slug
          #   @return [String] the slug of the current entity, in snake-case.
          field :slug, type: String

          validates :slug,
                    length: { minimum: 4, message: 'minlength', if: :slug? },
                    format: { with: /\A[a-z]+(_[a-z]+)*\z/, message: 'pattern', if: :slug? },
                    uniqueness: { message: 'uniq', if: :slug? },
                    presence: { message: 'required' }
        end
      end
    end
  end
end
