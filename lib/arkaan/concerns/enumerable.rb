# frozen_string_literal: true

module Arkaan
  module Concerns
    # Defines enumerations for the Mongoid models. An enumeration is a field that can only use a given set of values.
    # @author Vincent Courtois <courtois.vincent@outlook.com>
    module Enumerable
      extend ActiveSupport::Concern

      # Submodule holding all the static methods add to the current subclass.
      # @author Vincent Courtois <courtois.vincent@outlook.com>
      module ClassMethods
        # Creates the field with the given name, set of possible values, and options.
        # @param field_name [String] the name of the enumerated field.
        # @param values [Array<Symbol>] the possible values of the enumerated field.
        # @param options [Hash<Symbol, Any>] the possible options for the field.
        def enum_field(field_name, values, options = {})
          returned = field :"enum_#{field_name}", type: Symbol, default: options[:default]

          validates :"enum_#{field_name}", inclusion: { in: values.map(&:to_sym), message: 'inclusion' }

          define_getter(field_name)

          define_setter(field_name, values)

          define_values_methods(field_name, values)

          # This is to make enumerations historizable by
          # returning the field object created by Mongoid.
          returned
        end

        def define_getter(field_name)
          define_method field_name do
            return self["enum_#{field_name}"]
          end
        end

        def define_setter(field_name, values)
          define_method "#{field_name}=" do |value|
            value = value.to_sym
            self["enum_#{field_name}"] = value if values.include? value
          end
        end

        def define_values_methods(field_name, values)
          values.map(&:to_sym).each do |value|
            define_method "#{field_name}_#{value}!" do
              self["enum_#{field_name}"] = value
            end

            define_method "#{field_name}_#{value}?" do
              self["enum_#{field_name}"] == value
            end
          end
        end
      end
    end
  end
end
