# frozen_string_literal: true

module Arkaan
  # This module holds the logic for user authentication to our frontend.
  # @author Vincent Courtois <courtois.vincent@outlook.com>
  module Authentication
    autoload :Device, 'arkaan/authentication/device'
    autoload :Session, 'arkaan/authentication/session'
  end
end
