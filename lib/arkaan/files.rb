# frozen_string_literal: true

module Arkaan
  # This module holds all logic for the files uploaded in a conversation.
  # @author Vincent Courtois <courtois.vincent@outlook.com>
  module Files
    autoload :Document, 'arkaan/files/document'
    autoload :Permission, 'arkaan/files/permission'
  end
end
