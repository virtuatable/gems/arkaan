# frozen_string_literal: true

module Arkaan
  module Chatrooms
    # A membership is the link between a user and a conversation.
    # @author Vincent Courtois <courtois.vincent@outlook.com>
    class Membership
      include Mongoid::Document
      include Mongoid::Timestamps
      include Arkaan::Concerns::Enumerable

      enum_field :status, %i[shown hidden], default: :shown

      belongs_to :chatroom, class_name: 'Arkaan::Chatrooms::Private', inverse_of: :memberships

      belongs_to :account, class_name: 'Arkaan::Account', inverse_of: :memberships
    end
  end
end
