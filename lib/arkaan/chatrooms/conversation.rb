# frozen_string_literal: true

module Arkaan
  module Chatrooms
    # A conversation is an exchange of messages between several users.
    # @author Vincent Courtois <courtois.vincent@outlook.com>
    class Conversation < Arkaan::Chatrooms::Base
      has_many :memberships, class_name: 'Arkaan::Chatrooms::Membership', inverse_of: :chatroom
    end
  end
end
